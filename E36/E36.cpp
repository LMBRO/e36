// E36.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
using std::stack;
using std::string;
using std::to_string;
using std::cout;
using std::endl;

string dec_to_bin(int num) {
	if (num == 0) {
		return "0";
	}
	string bin{};
	while (num > 0) {
		bin += to_string(num % 2);
		num /= 2;
	}
	return string(bin.crbegin(), bin.crend());
}
bool is_palindrome(string s) {
	unsigned int len = s.size();
	if (len == 1)
		return true;
	else if (len == 0)
		return false;
	enum EO{EVEN, ODD};
	EO eo;
	if (len % 2 == 0)
		eo = EVEN;
	else
		eo = ODD;

	stack<char> st;
	unsigned int i = 0;
	while (i < len / 2) {
		st.push(s[i]);
		++i;
	}
	if (eo == ODD)
		++i;
	while (i < len) {
		if (st.top() != s[i]) {
			return false;
		}
		else {
			st.pop();
			++i;
		}
	}
	return true;
}
int main()
{
	unsigned int sum = 0;
	for (unsigned int i = 0; i < 1000000; ++i) {
		string si = to_string(i);
		if (!is_palindrome(si))
			continue;
		else {
			string sbi = dec_to_bin(i);
			if (is_palindrome(sbi)) {
				cout << si << "      " << sbi << endl;
				sum += i;
			}
		}
	}
	cout << "Sum is " << sum << endl;
	//cout << is_palindrome("11110100001000111111") << endl;
    return 0;
}

